using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject pin1;
    [SerializeField] private GameObject pin2;
    [SerializeField] private GameObject pin3;
    [SerializeField] private Button instrumentButton1;
    [SerializeField] private Button instrumentButton2;
    [SerializeField] private Button instrumentButton3;
    [SerializeField] private Text timerText;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject losePanel;

    private int _pin1Value;
    private int _pin2Value;
    private int _pin3Value;
    private int _instrument1Value;
    private int _instrument2Value;
    private int _instrument3Value;
    private float _remainingTime = 120;
    private bool _isTimerStopped;

    private void Start()
    {
        StartOrRestartGame();
    }

    private void Update()
    {
        if (_isTimerStopped) return;

        var secondsPassed = Time.deltaTime;

        if (_remainingTime <= 0)
        {
            _isTimerStopped = true;
            gamePanel.SetActive(false);
            losePanel.SetActive(true);
        }
        else
        {
            _remainingTime -= secondsPassed;
            SetTimerText();

            if (IsWin())
            {
                DisplayWinPanel();
            }
        }
    }

    private void InstrumentButton1Click()
    {
        InteractInstrumentsButton(false);
        
        if (_instrument1Value != 10)
        {
            ++_instrument1Value;
        }

        if (_instrument2Value != 0)
        {
            --_instrument2Value;
        }
        
        SetInstrumentsText();
    }

    private void InstrumentButton2Click()
    {
        InteractInstrumentsButton(false);
        
        if (_instrument1Value != 0)
        {
            --_instrument1Value;
        }

        if (_instrument2Value != 10)
        {
            if (_instrument2Value == 9)
            {
                ++_instrument2Value;
            }
            else
            {
                _instrument2Value += 2;
            }
        }

        if (_instrument3Value != 0)
        {
            --_instrument3Value;
        }
        
        SetInstrumentsText();
    }

    private void InstrumentButton3Click()
    {
        InteractInstrumentsButton(false);
        
        if (_instrument1Value != 0)
        {
            --_instrument1Value;
        }

        if (_instrument2Value != 10)
        {
            ++_instrument2Value;
        }

        if (_instrument3Value != 10)
        {
            ++_instrument3Value;
        }
        
        SetInstrumentsText();
    }

    private void StartOrRestartGame()
    {
        _pin1Value = Random.Range(1, 10);
        _pin2Value = Random.Range(1, 10);
        _pin3Value = Random.Range(1, 10);

        _instrument1Value = Random.Range(1, 10);
        _instrument2Value = Random.Range(1, 10);
        _instrument3Value = Random.Range(1, 10);

        if (IsWin())
        {
            StartOrRestartGame();
        }

        losePanel.SetActive(false);
        winPanel.SetActive(false);
        gamePanel.SetActive(true);
        
        _remainingTime = 120;
        
        SetTimerText();
        SetPinsText();
        SetInstrumentsText();
        
        _isTimerStopped = false;
    }

    private void SetPinsText()
    {
        pin1.GetComponent<Image>().fillAmount = _pin1Value / 10f;
        pin2.GetComponent<Image>().fillAmount = _pin2Value / 10f;
        pin3.GetComponent<Image>().fillAmount = _pin3Value / 10f;
    }

    private void SetInstrumentsText()
    {
        instrumentButton1.image.fillAmount = _instrument1Value / 10f;
        instrumentButton2.image.fillAmount = _instrument2Value / 10f;
        instrumentButton3.image.fillAmount = _instrument3Value / 10f;
        
        InteractInstrumentsButton(true);
    }

    private bool IsWin()
    {
        return _pin1Value == _instrument1Value
               && _pin2Value == _instrument2Value
               && _pin3Value == _instrument3Value;
    }

    private void SetTimerText()
    {
        timerText.text = $"{Mathf.RoundToInt(_remainingTime)}";
    }

    private void DisplayWinPanel()
    {
        _isTimerStopped = true;
        winPanel.SetActive(true);
        gamePanel.SetActive(false);
    }

    private void InteractInstrumentsButton(bool interactable)
    {
        instrumentButton1.interactable = interactable;
        instrumentButton2.interactable = interactable;
        instrumentButton3.interactable = interactable;
    }
}